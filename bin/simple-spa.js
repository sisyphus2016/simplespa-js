/*
 ********** simple-spa **********
 ${A simple SPA router}
 Project Home: http://git.oschina.net/guihuo/simplespa-js
 Author: guihuo
 Licence: MIT License
 Version: 0.1.1
 */

(function () {
    let simple_spa_context = {
        states: {},
        currentState: 'main-view',
        mainViewId: 'main-view',
        viewClass: 'view',
        init: function () {
            var views = document.getElementsByClassName(this.viewClass);
            for (var i = 0; i < views.length; i++) {
                var value = views[i];
                this.states[value.getAttribute("data-view-id")] = value.getAttribute('data-title');
                value.style.display = 'none';
            }
            this.currentState = window.location.hash.substring(1);
            if (this.currentState === "" || !this.checkState(this.currentState)) {
                this.currentState = this.mainViewId;
                window.location.hash = "#" + this.currentState;
            }
            document.title = this.states[this.currentState];
            this.getView(this.currentState).style.display = "block";
        },
        checkState: function (nextState) {
            for (var element in this.states) {
                if (element === nextState) {
                    return true;
                }
            }
            return false;
        },
        getView(viewId) {
            let viewList = this.getViewList();
            for (let i = 0; i < viewList.length; i++) {
                let view = viewList[i];
                if (view.getAttribute("data-view-id") === viewId) {
                    return view;
                }
            }
        },
        getViewList() {
            return document.getElementsByClassName(this.viewClass);
        }
    };


    window.Simple.SPA = {
        init: function (conf) {
            var p = simple_spa_context;
            if (conf == undefined) conf = {};
            if (conf.mainView !== undefined) {
                p.mainViewId = conf.mainView
            }
            if (conf.viewClass !== undefined) {
                p.viewClass = conf.viewClass
            }
            p.init();
            window.addEventListener("hashchange", function () {
                var nextState;
                if (window.location.hash == "") {
                    nextState = p.mainViewId;
                } else {
                    nextState = window.location.hash.substring(1);
                }
                console.log(nextState + " " + p.checkState(nextState));
                if (!p.checkState(nextState)) {
                    window.location.hash = "#" + p.currentState;
                    return;
                }
                document.title = p.states[nextState];
                p.getView(p.currentState).style.display = "none";
                p.getView(nextState).style.display = "block";
                p.currentState = nextState;
            });
        },
        addView: function (viewId, viewTitle) {
            if (!viewId) return false;
            if (!viewTitle) viewTitle = "";
            var p = simple_spa_context;

            if (!p.checkState(viewId)) {
                p.states[viewId] = viewTitle;
                return true;
            }
            return false;
        },
        removeView: function (viewId) {
            if (!viewId) return false;
            var p = simple_spa_context;
            delete p.states[viewId];
            return true;
        }
    }
})();