window.Simple = {
    resource: {
        /**
         * 加载js资源
         * @param url
         * @param callback
         */
        loadScript: function (url, callback) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if (typeof(callback) !== "undefined") {
                if (script.readyState) {
                    script.onreadystatechange = function () {
                        if (script.readyState === "loaded" || script.readyState === "complete") {
                            script.onreadystatechange = null;
                            callback(script);
                        }
                    };
                } else {
                    script.onload = function () {
                        callback(script);
                    };
                }
            }
            script.src = url;
            document.head.appendChild(script);
        },
        /**
         * 加载css资源
         * @param url
         * @param callback
         */
        loadStyle: function (url, callback) {
            var style = document.createElement("link");
            style.type = "text/css";
            style.rel = "stylesheet";
            style.href = url;
            if (typeof(callback) !== "undefined") {
                if (style.readyState) {
                    style.onreadystatechange = function () {
                        if (style.readyState === "loaded" || script.readyState === "complete") {
                            style.onreadystatechange = null;
                            callback(style);
                        }
                    };
                } else {
                    style.onload = function () {
                        callback(style);
                    };
                }
            }
            document.head.appendChild(style);
        },
        /**
         * 加载内容资源 如json
         * @param url
         * @param callback
         */
        loadContentFromUrl: function (url, method, callback, noCache) {
            if (noCache === undefined) noCache = true;
            var xmlhttp;
            if (!method) {
                method = 'GET';
            }
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                try {
                    xmlhttp = new XMLHttpRequest();
                } catch (e) {
                    return null;
                }
            }
            xmlhttp.open(method, url);
            if (noCache) {
                xmlhttp.setRequestHeader('Cache-Control', 'max-age=0');
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        callback(null, xmlhttp.responseText);
                    } else {
                        callback(xmlhttp.status, xmlhttp.responseText);
                    }
                }
            };
            try {
                xmlhttp.send(null);
            } catch (e) {
                callback(e, '');
            }
        },
        /**
         * 加载预加载资源
         * @param url
         * @param callback
         */
        loadPrefetch: function (url, callback) {
            //预读资源
            var link = document.createElement("link");
            link.rel = "prefetch";
            link.href = url;
            var called = false;
            var call = function () {
                if (!called) {
                    called = true;
                    callback(link);
                }
            };
            if (typeof(callback) !== "undefined") {
                if (link.readyState) {
                    link.onreadystatechange = function () {
                        if (link.readyState === "loaded" || link.readyState === "complete") {
                            link.onreadystatechange = null;
                            call();
                        }
                    };
                } else {
                    link.onload = function () {
                        call();
                    };
                }
            }
            setTimeout(function () {
                call();
            }, 1000);

            document.head.appendChild(link);
        },
    },
    browser: {
        Opera: "Opera",
        FireFox: "FireFox",
        Chrome: "Chrome",
        IE: "IE",
        /**
         * 取得浏览器的userAgent字符串
         * @returns {string}
         */
        getBrowser: function () {
            let userAgent = navigator.userAgent;
            if (userAgent.indexOf("Opera") > -1) {
                return "Opera"
            }
            if (userAgent.indexOf("Firefox") > -1) {
                return "FireFox";
            }
            if (userAgent.indexOf("Chrome") > -1) {
                return "Chrome";
            }
            if (userAgent.indexOf("Safari") > -1) {
                return "Safari";
            }
            if (!!window.ActiveXObject || "ActiveXObject" in window) {
                return "IE";
            }
        },
        isOpera: function () {
            return this.getBrowser() === this.Opera;
        },
        isFireFox: function () {
            return this.getBrowser() === this.FireFox;
        },
        isChrome: function () {
            return this.getBrowser() === this.Chrome;
        },
        isIE: function () {
            return this.getBrowser() === this.IE;
        },

    },
    date: {
        format: function (date, patten) {
            if (!fmt) {
                fmt = 'yyyy-MM-dd hh:mm:ss';
            }
            var o = {
                "M+": this.getMonth() + 1, //月份
                "d+": this.getDate(), //日
                "h+": this.getHours(), //小时
                "m+": this.getMinutes(), //分
                "s+": this.getSeconds(), //秒
                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                "S": this.getMilliseconds() //毫秒
            };
            if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        }

    }


};
