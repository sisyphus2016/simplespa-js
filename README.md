# simple-spa

> simple-spa是一个轻量级单页面应用路由工具

- 快速上手
- 无需任何第三方依赖
- 适合做少量个视图的小型单页面应用
- 基于window.location.hash轻量级实现

### simple-spa.js 使用帮助

##### simple.js的引入
```
<script type="text/javascript" src="simple-core.js"></script>
<script type="text/javascript" src="simple-spa.js"></script>
```

##### 插件初始化
&gt; 无参初始化
```
SimpleSPA.init();
```
##### 更多教程 版本稳定后更新